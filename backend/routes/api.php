<?php

//use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(/**
 * @param $router
 */
    [

    'middleware' => [
        'api',
        'cors',
    ],
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::patch('update', 'AuthController@update');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::get('token/refresh', 'AuthController@updateToken');

    Route::get('bakery/index', 'BakeryController@index'); // Show all
    Route::get('bakery/show', 'BakeryController@show'); // Find by id

    Route::post('service/bakery/f1we0hao6sad2ne38jfe7w9', 'FileController@jsonSave'); // Upload JSON File
    Route::get('service/bakery/test', 'BakeryController@index'); // Test
//    Route::get('service/bakery/add', 'BakeryController@add'); // Test
//    Route::post('service/bakery/add', 'BakeryController@store'); // Test

    Route::get('user/bakery/bycategories', 'BakeryController@getTOPProducts'); // Find by Categories
    Route::get('user/bakery/categories', 'BakeryController@getCategories'); // Show Categories
    Route::get('user/bakery/bytitle', 'BakeryController@getProducts'); // Find by title products
    Route::post('user/orders', 'OrderController@add'); // Buy orders
    Route::get('user/orders/user', 'OrderController@show'); // Get your orders by id user
    Route::post('user/orders/user', 'OrderController@cancel'); // Get your orders by id user
    Route::post('user/delivery/set', 'AdminController@setDelivery'); // Get user status delivery
    Route::get('user/delivery/get', 'AdminController@getDelivery'); // Set user status delivery

    Route::get('delivery/orders/reserve', 'OrderController@GetReserve'); // Get your orders by users
    Route::post('delivery/orders/reserve', 'OrderController@setReserved'); // Set your orders by delivery
    Route::get('delivery/orders/myreserved', 'OrderController@getMyReservedDelivery'); // Set your orders by delivery
    Route::post('delivery/orders/myreserved/cancel', 'OrderController@cancelDelivery'); // Set cancel order
    Route::post('delivery/orders/myreserved/accept', 'OrderController@acceptDelivery'); // Set accept order
    Route::get('admin/orders', 'OrderController@getAdminReservedDelivery'); // Set your orders by delivery
    Route::post('admin/orders/myreserved/remove', 'OrderController@adminRemoveDelivery'); // Set cancel order
    Route::post('admin/delivery/set', 'AuthController@setDelivery'); // Get user status delivery
    Route::get('admin/delivery/get', 'AuthController@getDelivery'); // Set user status delivery
    Route::get('admin/delivery/get/all', 'AuthController@getDeliveries'); // Set user status delivery


});


Route::group(/**
 * @param $router
 */
    [
    'middleware' => [
        'api',
        'cors',
    ],
    'prefix' => 'guestpart'

], function ($router) {
    Route::get('user/bakery/test', 'GuestBakeryController@getTest'); // Show Categories
    Route::post('user/orders', 'GuestBakeryController@add'); // Buy orders
    Route::get('user/orders/user', 'GuestBakeryController@show'); // Get your orders by id user
    Route::post('user/orders/user', 'GuestBakeryController@cancel'); // Get your orders by id user
});

Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found'], 404);
});
