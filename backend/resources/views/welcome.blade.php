<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>404</title>
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            body{
                display: flex;
                display: -moz-flex;
                display: -ms-flex;
                display: -webkit-flex;
                align-items: center;
                justify-content: center;
                background-color: black;
            }
            h1{
                text-decoration: underline;
                -webkit-text-decoration-color: yellow;
                text-decoration-color: yellow;
                color: red;
                position: absolute;
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                -moz-transform: translateY(-50%);
                transform: translateY(-50%);
            }
        </style>
    </head>
    <body>
        <h1 align="center"><span style="color: red;">404</span>NOT FOUND</h1>
    </body>
</html>
