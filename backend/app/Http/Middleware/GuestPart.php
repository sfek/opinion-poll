<?php

namespace App\Http\Middleware;

use Closure;
use App\GuestAuth;

/**
 * Class GuestPart
 * @package App\Http\Middleware
 */
class GuestPart
{
    /**
     * @param $data
     * @return bool
     */
    private function checkingLength($data)
    {
//        if (!empty($data)){
//            if (mb_strlen($data) >= 4) {
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
        return true;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tmp = $request->header('token');
        if ( $this->checkingLength($tmp) ){
            GuestAuth::checkToken($tmp);
            return $next($request);
//            return response()->json(
//                GuestAuth::checkToken($request->header('token')),
//                401
//            );
        } else {
//            return response()->json(['errors' => ['result' => 'Unauthorized']], 401);
            return response()->json(['errors' => ['result' => 'Invalid request']], 400);
        }
    }
}
