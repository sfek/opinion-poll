<?php

namespace App\Http\Controllers;

use App\User;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => [
            'login',
            'register',
            'refresh',
            'updateToken',
        ]]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email'   => 'required',
            'password'=> 'required',
        ]);
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['errors' => ['result' => 'Неверный логин или пароль']], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $request->validate([
            'name'    => 'required',
            'email'   => 'required',
            'telephone'   => 'required',
            'password'=> 'required|confirmed',
        ]);
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'telephone' => request('telephone'),
            'password' => Hash::make(request('password')),
        ]);
        return $this->login(request());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        auth()->user()->update($request->all());
        return response('update', Response::HTTP_ACCEPTED);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
//            'refresh_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user'       => auth()->user(),
        ]);
    }

    /**
     * @param User $user
     * @param Admin $admin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDelivery(User $user, Admin $admin, Request $request){
        if ($request->userStatus) {
            $admin->setDeliveryAdmin($request->_id, $request->userStatus);
            $firts = $admin->where('_id', $request->_id)->get();
            $result = $user->where('id', $firts[0]->userId)->update([
                'role' => 'delivery',
            ]);
        } else {
            $admin->setDeliveryAdmin($request->_id, $request->userStatus);
            $result = $admin->where('_id', $request->_id)->delete();
        }
        return response()->json($result, 200);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDelivery(User $user, Request $request){
        $obj = $user->find((int)$request->id);
        if ($obj) {
            $result = $obj->role;
        } else {
            $result = false;
        }
        return response()->json($result, 200);
    }

    /**
     * @param Admin $admin
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeliveries(Admin $admin){
        return response()->json( $admin->getDeliveries() );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateToken(Request $request):\Illuminate\Http\JsonResponse
    {
        try {
            return response()->json([
                'access_token' => auth()->refresh($request->bearerToken()),
                'token_type'   => 'bearer',
                'expires_in'   => auth()->factory()->getTTL() * 60,
                'user'         => auth()->user(),
                'result'       => 'Successfully',
                'msg'          => 'Токен получен',
            ], 200);
        } catch (TokenBlacklistedException $e){

            return response()->json(
                [
                    'result' => 'Locked',
                    'msg'    => 'Токен ранее уже был использован'
                ],
                423
            );

        } catch (TokenExpiredException $e) {

            return response()->json(
                [
                    'result' => 'Token expired',
                    'msg'    => 'Срок жизни токена истек'
                ],
                401
            );

        } catch (TokenInvalidException $e) {

            return response()->json(
                [
                    'result' => 'Not Acceptable',
                    'msg'    => 'Неизвестный токен'
                ],
                405
            );

        } catch (JWTException $e) {

            return response()->json(
                [
                    'result' => 'Client Error',
                    'msg'    => 'Необрабатываемый запрос'
                ],
                400
            );

        } catch (\Exception $e) {

            return response()->json(
                [
                    'result' => 'Unknown Error',
                    'msg'    => 'Неизвестная ошибка'
                ],
                400
            );
        }
    }
}
