<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GuestAuth
 * @package App
 */
class GuestAuth extends Eloquent
{
    /**
     * @var string
     */
    protected $connection = 'mongodb';
    /**
     * @var string
     */
    protected $collection = 'guestAuth';


    /**
     * @param $data
     * @return bool
     */
    static private function getToken($data)
    {
        $check = DB::connection('mongodb')
            ->collection('guestAuth')
            ->where('userId', (int)$data)
            ->get();

        if(sizeof($check) !== 0){
            return true; // if exist user then return true
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    static private function setToken($data)
    {
        return DB::connection('mongodb')
            ->collection('guestAuth')
            ->insert(
                ['userId' => (int)$data,
                'createdDate' => (now()->timestamp) * 1000,
                'status' => true]
            );
    }

    /**
     * @param $data
     */
    static public function checkToken($data)
    {
        if (!self::getToken($data)){
            self::setToken($data);
        }
    }
}
